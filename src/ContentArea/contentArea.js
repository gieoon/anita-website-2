import React from 'react';
import './contentArea.css';
import Carousel from 'react-bootstrap/Carousel';
import Slider from 'react-slick';
import '../App.css';
import {CatfishComponent, HouseflyComponent, IllustrationComponent, PrintsComponent, PhotographyComponent, TinyKimComponent, WowComponent, GacComponent} from './subpageComponent';
import {Footer} from '../HomePage/footer';

import c1 from '../res/01/IMG_9555.JPG.jpg';

class ContentArea extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			carouselImages: [],
			title: props.title,
			menuTitle: props.menuTitle
		}
	}

	componentDidMount(){
		//this.setContentAreaBySubPageAndMenuPage(this.props.title, this.props.menuTitle);
	}

	setContentAreaBySubPageAndMenuPage = (title, menuTitle) => {
		console.log("title: ", title);
		console.log("menutitle: ", menuTitle);
		switch(title){
			case'textiles':
				switch(menuTitle){
					case 'catfish':
						return <CatfishComponent/>
					case 'housefly':
						return <HouseflyComponent/>
					case 'prints':
						return <PrintsComponent/>
				}
				break;
			case 'illustration':
				return <IllustrationComponent/>
			case 'photography':
				return <PhotographyComponent/>
			case 'tinykim':
				return <TinyKimComponent/>

			case'work':
				switch(menuTitle){
					case 'wow':
						return <WowComponent/>
					case 'gac':
						return <GacComponent/>
				}
				break;


		}
	}

	render(){
		return(
			<div>
				{
					this.setContentAreaBySubPageAndMenuPage(this.props.title, this.props.menuTitle)
				}
				<Footer />
			</div>
		);
	}
}

export default ContentArea;

//https://react-slick.neostack.com/docs/api/
class CarouselComponent extends React.Component{
	render(){
		let settings = {
	      	dots: true,
	      	infinite: true,
	      	//fade: true,
	      	speed: 500,
	      	autoplay: true,
	      	autoplaySpeed: 2000,
	      	slidesToShow: 1,
	      	slidesToScroll: 1,
	      	//accessibility: true,
	      	accessibility: true,
	      	arrows: true
	    };
		
		return(
			<div style={{width:'70vw', margin: '2rem auto'}}>
				<Slider {...settings}>
					
					{
						this.props.images.map((image, index) => (
							<img key={index} src={image.src} style={{width: '50vw', height: '50%', backgroundSize: 'contain'}}/>
						))
					}
				</Slider>
				{
					/*
					<div style={{marginTop: '2rem'}}>
					{
						//mini images beneath the main carousel
						this.props.images.map((image, index) => (
							<div key={index} style={{display: 'inline-block'}}>
								<img style={{width: '10vw', height:'12vh'}} src={image.src} className="" alt={"minimage" + index.toString()}/>
							</div>
						))
					}
					</div>
					*/
				}
				
			</div>

		);

	}
}

const HorizontalImages = (props) => {
	return(
		<div>
		{
						console.log('props: ', props)
						
					}
		{
			props.images.map((image, index) => (
				<div key={index} 
					style={{display: 'inline-block', padding: '1.5rem'}} >
					<Image highlightable={props.highlightable} width={props.width[index] || props.width[0]} image={image}/>
				</div>
			))
		}
		</div>
	);
}

const Image = (props) => {
	return(
		<div>
			<div className={props.highlightable ? 'highlightable' : ''} >
				<a href={props.image.hyperlink ? props.image.hyperlink : ''}>
					<img style={{width: props.width}} src={props.image.src} alt={"minimage"}/>
				</a>
				<div className="highlightable-text"><span >{props.image.innerText}</span></div>
			</div>
			<span style={{display:'block', color:'rgba(0,0,0,.7)',fontSize:'.75rem'}}>{props.image.description}</span>
		</div>
	);
}
//style={{lineHeight: '7.5vw'}}

//draw two components side by side
const ComponentsSideways = (props) => {
	return(
		<div className="g-flex ">
			{
				props.components[0]
			}
			{
				props.components[1]
			}
		</div>
	);
}

export {CarouselComponent, HorizontalImages, Image, ComponentsSideways};


/*
{
						this.props.images.map((image, index) => (
							<img src={image.src} style={{width: '50vw'}}/>
						))
					}


*/
/*
return (
			<div>
				<Carousel>
				{
					this.props.images.map((image, index) => (
						<Carousel.Item>
							<img
								style={{width: '70vw'}}
								key={index}
								className="d-block w-100"
								//src={image.src}
								//src={c1 }
								src="holder.js/800x400?text=First slide&bg=373940"
								alt="First Slide"
								//alt={"slide" + index.toString()}
							/>
							<Carousel.Caption>
								<h3>{image.text}</h3>
								<p>{image.description}</p>
							</Carousel.Caption>	
						</Carousel.Item>
					))
				}
				</Carousel>

				{
					//mini images beneath the main carousel
					this.props.images.map((image, index) => (
						<div>
							<img src="" className="" alt={"minimage" + index.toString()}/>
						</div>
					))
				}
				
			</div>
		)

*/