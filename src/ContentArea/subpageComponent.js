
import React from 'react';
import './subpageComponent.css';
import { CarouselComponent, HorizontalImages, Image, ComponentsSideways } from './contentArea';

//catfish
import kim_catfish from '../res/01/kim catfish.jpg'
import c1 from '../res/01/IMG_9555.JPG.jpg';
import c2 from '../res/01/IMG_9559.JPG.jpg';
import c3 from '../res/01/IMG_9676_edited.jpg';
import c4 from '../res/01/IMG_9702-2_edited.jpg';
import c5 from '../res/01/IMG_9704-2_edited.jpg';
import c6 from '../res/01/IMG_9706-2_edited.jpg';
import c7 from '../res/01/IMG_9711-2_edited.jpg';
import c8 from '../res/01/waratah_1_dychinco.jpg';
import c9 from '../res/01/Screen Shot 2018-12-13 at 10.35.51 AM.png';

//illustration
import illustration1 from '../res/for portfolio/Facebook cover photo.webp';
import illustration2 from '../res/face.webp';

//prints
import prints1 from '../res/for portfolio/flower1.webp';
import prints2 from '../res/for portfolio/flower2.webp';
import prints3 from '../res/for portfolio/flower3.webp';
import prints4 from '../res/for portfolio/flower4.webp';

//photographymainmenu
import wellingtonmenu from '../res/wellington/IMG_8161.jpg';
import philippinesmenu from '../res/philippines/IMG_0360.jpg';
import simplybuildingmenu from '../res/simply building/house4-24.jpg';
import japanfestivalmenu from '../res/japan festival/IMG_0785.jpg';
import callalilymenu from '../res/calla lily/mainphoto1.webp';

//tinykim
import tinykim from '../res/tinykim/tinykimrecordp.webp';

//wow
import wow1 from '../res/Wow/original photos - Copy/wow1.jpg';
import wow2 from '../res/Wow/original photos - Copy/wow2.jpg';
import wow3 from '../res/Wow/original photos - Copy/wow3.jpg';
import wow4 from '../res/Wow/original photos - Copy/wow4.jpg';
import wow5 from '../res/Wow/original photos - Copy/wow5.jpg';
import wow6 from '../res/Wow/original photos - Copy/wow6.jpg';
import wow7 from '../res/Wow/original photos - Copy/wow7.jpg';
import wow8 from '../res/Wow/original photos - Copy/wow8.jpg';
import wow9 from '../res/Wow/original photos - Copy/wow9.jpg';
import wow10 from '../res/Wow/original photos - Copy/wow10.jpg';
import wow11 from '../res/Wow/original photos - Copy/wow11.jpg';
import wow12 from '../res/Wow/original photos - Copy/wow12.jpg';
import wow13 from '../res/Wow/original photos - Copy/wow13.jpg';
import wow14 from '../res/Wow/original photos - Copy/wow14.jpg';
import wow15 from '../res/Wow/original photos - Copy/wow15.jpg';
import wow16 from '../res/Wow/original photos - Copy/wow16.jpg';
import wow17 from '../res/Wow/original photos - Copy/wow17.jpg';
import wow18 from '../res/Wow/original photos - Copy/wow18.jpg';
import wow19 from '../res/Wow/original photos - Copy/wow19.jpg';
import wow20 from '../res/Wow/original photos - Copy/wow20.jpg';
import wow21 from '../res/Wow/original photos - Copy/wow21.jpg';
import wow22 from '../res/Wow/original photos - Copy/wow22.jpg';
import wow23 from '../res/Wow/original photos - Copy/wow23.jpg';
import wow24 from '../res/Wow/original photos - Copy/wow24.jpg';
import wow25 from '../res/Wow/original photos - Copy/wow25.jpg';
import wow26 from '../res/Wow/original photos - Copy/wow26.jpg';
import wow27 from '../res/Wow/original photos - Copy/wow27.jpg';
import wow28 from '../res/Wow/original photos - Copy/wow28.jpg';
import wow29 from '../res/Wow/original photos - Copy/wow29.jpg';
import wow30 from '../res/Wow/original photos - Copy/wow30.jpg';
import wow31 from '../res/Wow/original photos - Copy/wow31.jpg';
import wow32 from '../res/Wow/original photos - Copy/wow32.jpg';
import wow33 from '../res/Wow/original photos - Copy/wow33.jpg';



const titleStyle = {
	letterSpacing: '0.1em',
	fontSize: '27px',
	fontFamily: 'courier new,courier-ps-w01,courier-ps-w02,courier-ps-w10,monospace',

}


const standardImg = {
	width: '70vw',
	paddingTop: '2rem'
}

const imgLabel = {
	textAlign: 'center',
	fontSize: '12px'
};

export const CatfishComponent = (props) => {
	return(
		<div className="content-area">
			<CarouselComponent images={[
				{src: c1, text: '', description: ''}, 
				{src: c2}, 
				{src: c3},
				{src: c4},
				{src: c5}, 
				{src: c6}
			]}/>
			<div style={{marginTop: '5rem'}}>
				<p></p>
				<span style={titleStyle}>01: Catfish</span>
				<br/><br/>
				<p>
					01 is a digitally printed unisex collection inspired by “catfish”, a term used to describe someone in social media and online dating apps that pretends to be someone they are not.
				</p> 	
				<br/>		
				<p>
					This collection shows an abstract representation of two different perspectives: the ‘catfish’ and the 'catfished' in a struggle between the digital and the real world through a combination of photography, hand and digitally illustrated images made to communicate and show an understanding of what it feels to be in their position. It revolves around the idea of deception, loneliness, desperation to connect, be loved and to seek love from others.
				</p>
				<div>
					<img style={standardImg} src={kim_catfish} alt="kim catfish"/>
				</div>
				<p style={imgLabel}>Visualisation Board</p>
				<div style={{marginTop: '5rem'}}>
					<CarouselComponent images={[
						{src: c7}, {src:c8}, {src:c9}
					]}/>
				</div>
				<br/>
				<p>
					I took photos of myself and digitised them on photoshop. I scanned different fabrics and transformed made them digitally, playing around with the idea of masking and filtering to represent the concept of concealing and deception.
				</p>
			</div>
		</div>
	);
}

export const HouseflyComponent= (props) => {
	return(
		<div className="content-area g-grid">
			<CarouselComponent  images={[
				{src: c1}, {src: c2}
			]} />
			<div style={{marginTop: '2rem'}}>
				<p></p>
				<span style={titleStyle}></span>
				<p></p>
				<p>
					"Housefly" is an avant garde collection that targets a market of free-spirited people who want to be visible both during the day and night with the use of unusual structures and retroreflective threads that keep them noticeable everywhere they go.
				</p>
				<br/>
				<p>
					This is my mood board for this collection. I am taking inspiration from the qualities of a housefly: Filthy, Reflective Optical Illusion eyes, The wings.
				</p>
				<br/>
				<p>
					Another inspiration I'm getting from for the structure is Autotomy, where it has the ability to transform or separate or move.
				</p>
				<br/>
				<p>
					Ideally, it's supposed to be made into a transforming garment, sadly, it takes hours to weave an A4 size sample. 
				</p>
				<br/>
				<HorizontalImages width={['15vw']} images={[
					{src: c1, description: 'description'}, {src: c2, description: 'description'}, {src: c2, description: 'another description'}
				]}/>
				<p>
					It glows in the dark because of the retroreflective threads woven, sewn on and felted in them.
				</p>
				<br/>
				<p>
					I have been on purposely knitting holes to attempting to create the concept of the housefly trapped in a spiderweb.
				</p>
				<br/>
				<p>
					Knotting and experimenting on what happens if I insert a tube inside the knitted pleats I created 
				</p>
				<br/>
				<p>
					Laser cut
				</p>
				<br/>
				<p>
					First layer of wool for felting
				</p>
				<br/>
				<Image width={'25vw'} image={{
					src: c8,
					description: 'random description here'
				}}/>

			</div>
		</div>
	);
}

export const IllustrationComponent = (props) => {
	return(
		<HorizontalImages width={['40vw', '20vw']} images={[
			{src: illustration1, description: ''}, {src: illustration2, description: ''}
		]}/>		
	);
}

export const PrintsComponent = (props) => {
	return(
		<div>
			<HorizontalImages width={['15vw']} images={[
				{src: prints1, description: ''}, {src: prints2, description: ''}, {src: prints3, description: ''}, {src: prints4, description: ''}
			]}/>
			<p>
				A Flower
			</p>
​
			<p>
				I have always liked the child-like drawings because they give off a happier vibe. I was inspired by the girls wearing kimonos during the Japan Festival. 
			</p>
		</div>		
	);
}

export const PhotographyComponent = (props) => {
	return(
		<div>
			<HorizontalImages width={['15vw']} highlightable={true} images={[
				{src: wellingtonmenu, description: ''}, {src: philippinesmenu, description: ''}, {src: simplybuildingmenu, description: ''}
			]}/>
			<HorizontalImages width={['15vw']} highlightable={true} images={[
				{src: japanfestivalmenu, description: '', hyperlink: '/', innerText:'on the stage doing stuff'}, {src: callalilymenu, description: '', hyperlink: '/textiles'}
			]}/>
		</div>
	);
}

export const TinyKimComponent = (props) => {
	//<img style={{width: '40vw', marginLeft: '10rem'}} src={tinykim}/>	
	//<div style={{textAlign:'left'}}>
	return(
		<div>	
			<ComponentsSideways components={[
				<Image width={'25vw'} image={
					{src: tinykim, description: 'random description here'}
				}/>,
				<div>
					<p>
					"Tiny Kim" is a personal project of mine that started when I thought of making handmade gifts for my friend. I used materials that were leftovers from my projects and things that I already had, like paper, because I was a cheapskate. 
					</p>
					<p>
					I decided to take photos of these before I gave it away and realised that I could do a stopmotion animation of it.
					</p>
				</div>
			]}/>
					
					
			
			<br/>
			<br/>

			<span>
			Episode 1: "The Record Player"
			</span>
			<br/>
			<br/>

			<iframe width="560" height="315" src="https://www.youtube.com/embed/LEL2vzYbwkk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

			<br/>
			<br/>
			<span>
			I upload new episodes of tiny randomness every 1st of the month.
			</span>

		</div>
	);
}

export const WowComponent = (props) => {
	return(
		<div>
			<CarouselComponent  images={[
				{src: wow1}, {src: wow2}, {src: wow3}, {src: wow4}, {src: wow5}, {src: wow6}, {src: wow7}, {src: wow8}, {src: wow9}, {src: wow10}, {src: wow11}, {src: wow12}, {src: wow13}, {src: wow14}, {src: wow15}, {src: wow16}, {src: wow17}, {src: wow18}, {src: wow19}, {src: wow20}, {src: wow21}, {src: wow22}, {src: wow23}, {src: wow24}, {src: wow25}, {src: wow26}, {src: wow27}, {src: wow28}, {src: wow29}, {src: wow30}, {src: wow31}, {src: wow32}, {src: wow33}
			]} />

			<br/>
			<br/>
			<p>
				WOW Wander 2018 interns are a team of 9 design students from Massey University Wellington. We were tasked to design installations for 16 garments from the WOW winners in the previous years across the key tourist areas around the Wellington City Central.
			</p>
			<br/>
			<br/>
			<a href={'/work'}>
				<p style={{textDecoration:'underline', fontSize: '.9rem'}}>
				Featured in: stuff.co.nz
				</p>
			</a>
			
			<br/>
			<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FWorldofWearableArt%2Fvideos%2F1785797074863017%2F&show_text=0&width=560" width="560" height="315"  scrolling="no" frameBorder="0" />

		</div>
	);
}

export const GacComponent = (props) => {
	return(
		<div>
		</div>
	);
}

export const ComponentTemplate = (props) => {
	return(
		<div>
		</div>
	);
}

