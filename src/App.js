import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './g.css';

import SubPage from './SubPage/subPage.js';
import HomePage from './HomePage/homePage.js';

class App extends Component {
  render() {
    return (
      <div className="App">
        <HomePage />
      </div>
    );
  }
}


export default App;
