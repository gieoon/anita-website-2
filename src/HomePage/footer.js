import React from 'react';
import fb from '../res/fb logo.webp';
import linkedin from '../res/linkedin logo.webp';
import instag from '../res/instag logo.webp';
import yt from '../res/yt logo.webp';

export const Footer = (props) => {
	return(<div className="links">
		<a target="_blank" href="https://www.facebook.com/TinyKimCo/"><img src={fb} alt="fb" /></a>
		<a target="_blank" href="https://www.linkedin.com/in/deanne-dychinco-62309a119/"><img src={linkedin} alt="linkedin" /></a>
		<a target="_blank" href="https://www.youtube.com/channel/UCEjgcHf6ND1__tLvNlDgR5A"><img src={yt} alt="yt" /></a>
		<a target="_blank" href="https://www.instagram.com/the.tiny.kim/"><img src={instag} alt="insta" /></a>
	</div>);	
}