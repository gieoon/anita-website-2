import React from 'react';
import {Footer} from './footer';
import title from '../res/deanne is pro.webp';
import mainImg from '../res/face.webp';
import textiles from '../res/textiles.webp';
import fashion from '../res/fash.webp';
import illustrations from '../res/illustrations.webp';
import photography from '../res/photography.webp';
import tinykim from '../res/tinykim1.webp';
import work from '../res/work.webp';

class HomePage extends React.Component {
	constructor(props){
		super(props);
		this.state = {

		}
	}

	render(){
		return(
			<div className="">
				<div className="title-area">
					<img src={title} alt="title"  />
				</div>
				<div className="main-area">
					<img src={mainImg} alt="mainImg"  />
				</div>
				<table>
					<tbody>
						<tr>
							<td>
								<a href="/textiles">
									<img src={textiles} alt="textiles"/>
								</a>
							</td>
							<td>
								<a href="/fashion">
									<img src={fashion} alt="fashion"/>
								</a>
							</td>
							<td>
								<a href="/illustration">
									<img src={illustrations} alt="illustrations"/>
								</a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="/photography">
									<img src={photography} alt="photography"/>
								</a>
							</td>
							<td>
								<a href="/tinykim">
									<img src={tinykim} alt="tinykim"/>
								</a>
							</td>
							<td>
								<a href="/work">
									<img src={work} alt="work"/>
								</a>
							</td>
						</tr>
					</tbody>
				</table>	
				<Footer />	
			</div>
		);
	}
} 

export default HomePage;