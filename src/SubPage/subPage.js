import React from 'react';
import './subPage.css'
import ContentArea from '../ContentArea/contentArea.js';
//titles
import textiles from '../res/subpages/textiles.webp';
import fashion from '../res/subpages/fash.webp';
import illustration from '../res/for portfolio/illustrations.png'
import photography from '../res/for portfolio/photography.png'
import tinykim from '../res/for portfolio/tinykim1.png';
import work from '../res/for portfolio/work.png';

//generic menu items
import underline from '../res/subpages/under.webp';
import home from '../res/subpages/home text.webp';

//textile menu
import catfish from '../res/subpages/01catfish.webp';
import housefly from '../res/subpages/housefly.webp';
import prints from '../res/subpages/prints.webp';

//work menu
import wow from '../res/for portfolio/wow.webp';
import gac from '../res/for portfolio/gac.webp';

class SubPage extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			titleText: this.props.match.params.title,
			menuItems: [],
			selectedMenuItemIndex: 0,
			selectedMenuItemString: ''
		}
		
	}

	componentDidMount(){
		this.setMenuImageFromURL();
	}

	setTitleImageFromURL = () => {
		switch(this.state.titleText){
			case 'textiles':
				return textiles;
			case 'fashion':
				return fashion;
			case 'illustration':
				return illustration;
			case 'photography':
				return photography;
			case 'tinykim':
				return tinykim;
			case 'work':
				return work;
			default :
				return undefined;
		}
	}

	setMenuImageFromURL = () => {
		switch(this.state.titleText){
			case 'textiles':
				this.setState({
					menuItems: [{"home": home}, {"catfish": catfish}, {"housefly": housefly}, {"prints": prints}],
					selectedMenuItemString: 'catfish'
				}, () => {
					console.log("updated state: ", this.state.selectedMenuItemString);
				});
				break;
			case 'fashion':
				this.setState({
					menuItems: [{"home": home}]
				});
				break;
			case 'illustration':
				this.setState({
					menuItems: [{"home": home}]
				});
				break;
			case 'photography':
				this.setState({
					menuItems: [{"home": home}]
				});
				break;
			case 'tinykim':
				this.setState({
					menuItems: [{"home": home}]
				});
				break;
			case 'work':
				this.setState({
					menuItems: [{"home": home}, {"wow": wow}, {"gac": gac}],
					selectedMenuItemString: 'wow'
				}, () => {
					console.log("updated state: ", this.state.selectedMenuItemString);
				});
				break;
		} 
	}

	setMenuImgHrefFromString = (string) => {
		switch(string){
			case 'home':
				//console.log("setting url");
				return '/';
		}
	} 

	clickedMenuItemUpdateContentArea = (item, index) => {
		//console.log("cliekd on item: ", item);
		this.setState({
			selectedMenuItemString: Object.keys(item)[0],
			selectedMenuItemIndex: index - 1
		});
	}


	render(){
		if(this.state.menuItems.length > 0) console.log('this.state.menuItems[index]: ', this.state.menuItems[0][Object.keys(this.state.menuItems[0])[0]])
		return(
			<div className="App">
				<img className="title-img" src={this.setTitleImageFromURL()}/>
				<div>
				{
					this.state.menuItems.map((item, index) => (
						<a className="a-wrapper" key={index} href={this.setMenuImgHrefFromString(Object.keys(item)[0])}>
							
							<div className="menu-container">
								<img src={
									item[Object.keys(item)[0]]
								} className="menu-item"
								onClick={() => this.clickedMenuItemUpdateContentArea(item, index)}/>
								{
									this.state.selectedMenuItemIndex + 1 === index &&
									<div className="underline">
										<img src={underline} alt="under" />
									</div>
								}
							</div>
						</a>
					))
				}
				</div>
				<ContentArea 
					title={this.state.titleText}
					menuTitle={this.state.selectedMenuItemString}
				/>
			</div>
		);
	}
}	

export default SubPage;

